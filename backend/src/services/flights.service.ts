import { FlightsModel } from '../models/flights.model'

export class FlightsService {
    async getAll() {
        return await FlightsModel.find()
    }

    async getFirst(field: string, value: any) {
        return await FlightsModel.findOne({ [field]: value })
    }

    async updateById(id: string, field: any, value: any) {
        try {
            const flight = await this.getFirst('id', id)
            if (!flight) {
                throw new Error('Flight was not found')
            }

            if (!flight[field]) {
                throw new Error('Field is not valid')
            }

            flight[field] = value
            return await flight.save()
        } catch (e: any) {
            return { success: false, message: e }
        }
    }
}
