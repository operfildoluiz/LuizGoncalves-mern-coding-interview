import { JsonController, Get, Param, Put, Body } from 'routing-controllers'
import { FlightsService } from '../services/flights.service'

const flightsService = new FlightsService()

@JsonController('/flights', { transformResponse: false })
export default class FlightsController {
    @Get('')
    async getAll() {
        return {
            status: 200,
            data: await flightsService.getAll(),
        }
    }

    @Get('/:code')
    async getFlight(@Param('code') code: string) {
        return {
            status: 200,
            data: await flightsService.getFirst('code', code),
        }
    }

    @Put('/:code')
    async updateFlight(@Param('code') code: string, @Body() flight: any) {
        console.log(flight)
        /*return {
            status: 200,
            data: await flightsService.updateById('code', code),
        }*/
    }
}
